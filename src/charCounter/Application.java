package charCounter;

import java.util.Map;

public class Application {
    public static void main(String[] args){
        String inputFile = "src/charCounter/text.txt";
        String outputFile = "src/charCounter/result.txt";

        Map<Character, Integer> map = FileExecutor.readFile(inputFile);
        Map<Character, Integer> resultMap = FileExecutor.sortByValue(map);

        FileExecutor.insertResult(resultMap, outputFile);
    }
}
