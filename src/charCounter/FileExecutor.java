package charCounter;

import java.io.*;
import java.util.*;

public class FileExecutor{

    public static Map<Character, Integer> readFile(String fileName){
        Map<Character, Integer> map = new HashMap<>();

        FileInputStream inputStream = null;

        try{
            inputStream = new FileInputStream(fileName);

            while (inputStream.available() > 0){
                char cymbol = (char) inputStream.read();

                if(map.containsKey(cymbol)){
                    map.put(cymbol, map.get(cymbol) + 1);
                } else {
                    map.put(cymbol, 1);
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return map;
    }

    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map){
        List<Map.Entry<K, V>> list = new LinkedList<>(map.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
            @Override
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                return o1.getValue().compareTo(o2.getValue());
            }
        });

        Map<K, V> result = new LinkedHashMap<K, V>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }


    public static void insertResult(Map<Character, Integer> map, String fileName){

        FileWriter outputStream = null;
        try {
            outputStream = new FileWriter(fileName);

            for (Map.Entry<Character, Integer> m : map.entrySet()) {
                String res = "\" " + m.getKey() + " \" = " + m.getValue() + "\n";
                outputStream.write(res);
            }

            outputStream.close();
        } catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}